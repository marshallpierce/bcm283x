#![feature(pointer_methods, try_from)]

//! You're going to want to crack open the [peripherals documentation](https://www.raspberrypi.org/app/uploads/2012/02/BCM2835-ARM-Peripherals.pdf) and the [errata](https://elinux.org/BCM2835_datasheet_errata).

#[macro_use]
extern crate lazy_static;

pub mod dma;
pub mod linux;

//! Register management for one particular channel.

use linux::{AlignedAddressPair, PhysicalAddr};
use super::is_bit_set;
use super::control_status::*;
use super::control_block::*;
use super::tx_info::*;

// Register offsets in units of *u32 (4 bytes)
pub(crate) const REG_CTRL_STATUS: isize = 0;
const REG_CTRL_BLK_ADDR: isize = 1;
const REG_TX_INFO: isize = 2;
const REG_SRC_ADDR: isize = 3;
const REG_DST_ADDR: isize = 4;
const REG_TX_LEN: isize = 5;
const REG_STRIDE: isize = 6;
const REG_NEXT_CTRL_BLK_ADDR: isize = 7;
const REG_DEBUG: isize = 8;

/// Virtual address mapped to a DMA channel's registers
pub(crate) struct VirtualRegister {
    /// Virtual address at start of channel's registers
    pub(crate) base_addr: *mut u32,
}

impl VirtualRegister {}

/// Wrapper around the registers for a DMA channel.
pub trait RegisterSet<'a, 'b> {
    type View: 'a + RegisterView;
    type Write: 'b + RegisterWriter;

    fn view(&'a self) -> Self::View;
    fn write(&'b mut self) -> Self::Write;
}

/// DMA registers for a Normal channel.
pub struct RegisterSetNormal {
    pub(crate) register: VirtualRegister,
}

impl<'a, 'b> RegisterSet<'a, 'b> for RegisterSetNormal {
    type View = RegisterViewNormal<'a>;
    type Write = RegisterWriterNormal<'b>;

    fn view(&'a self) -> Self::View {
        RegisterViewNormal {
            lite: RegisterViewLite {
                register: &self.register,
            },
        }
    }
    fn write(&'b mut self) -> Self::Write {
        RegisterWriterNormal {
            lite: RegisterWriterLite {
                register: &mut self.register,
            },
        }
    }
}

/// DMA registers for a Lite channel.
pub struct RegisterSetLite {
    pub(crate) register: VirtualRegister,
}

impl<'a, 'b> RegisterSet<'a, 'b> for RegisterSetLite {
    type View = RegisterViewLite<'a>;
    type Write = RegisterWriterLite<'b>;

    fn view(&'a self) -> Self::View {
        RegisterViewLite {
            register: &self.register,
        }
    }

    fn write(&'b mut self) -> Self::Write {
        RegisterWriterLite {
            register: &mut self.register,
        }
    }
}

/// Read DMA register state.
///
/// Reads from the registers are performed when these methods are invoked, so structs returned
/// are simply snapshots to decode the data and will not change.
pub trait RegisterView {
    type TxInfo;
    type TxLen;
    unsafe fn ctrl_status(&self) -> ControlStatus;
    unsafe fn ctrl_blk_addr(&self) -> Option<PhysicalAddr>;
    unsafe fn tx_info(&self) -> Self::TxInfo;
    /// Updated as the transfer progresses.
    unsafe fn src_addr(&self) -> Option<PhysicalAddr>;
    /// Updated as the transfer progresses.
    unsafe fn dst_addr(&self) -> Option<PhysicalAddr>;
    /// Transfer length remaining. The register is updated as the DMA engine, so calling this
    /// multiple times will show progress.
    unsafe fn tx_len(&self) -> Self::TxLen;
    unsafe fn next_ctrl_blk_addr(&self) -> Option<PhysicalAddr>;
    unsafe fn debug(&self) -> Debug;
}

/// Write DMA register state.
pub trait RegisterWriter {
    type CtrlBlock;
    /// Returns a helper to write to the control status register.
    fn ctrl_status(&mut self) -> ControlStatusUpdate;
    /// Write the control block to the given address and write the control block address register.
    // TODO How to handle CB lifetimes? Own address on creation?
    unsafe fn ctrl_blk_addr(&mut self, block: &Self::CtrlBlock, addr: &AlignedAddressPair);
    /// Write the control block to the given address and write the next control block address
    /// register.
    ///
    /// Only use when the DMA channel is paused. Must be 256-bit aligned.
    unsafe fn next_ctrl_blk_addr(&mut self, block: &Self::CtrlBlock, addr: &AlignedAddressPair);
    /// Returns a helper to write to the debug register.
    // TODO
    fn debug(&mut self);
}

/// Transfer length for stride-capable Normal DMA channels.
#[derive(Debug, Copy, Clone)]
pub struct TransferLength {
    pub(crate) len: u32,
}

impl TransferLength {
    /// `y` indicates how many `x`-length transfers are to be performed.
    ///
    /// `y` can be at most 14 bits.
    pub fn with_2d(x: u16, y: u16) -> TransferLength {
        // y can't have top 2 bits
        assert_eq!(0, y >> 14, "y is a 14 bit value");

        TransferLength {
            len: ((y as u32) << 16) | (x as u32),
        }
    }

    /// `len` can be at most 30 bits.
    pub fn with_length(len: u32) -> TransferLength {
        assert_eq!(0, len >> 30);

        TransferLength { len }
    }

    pub fn as_len(&self) -> u32 {
        self.len
    }

    /// Tuple of x length, y length. Y length is how many x length transfers are performed.
    pub fn as_2d(&self) -> (u16, u16) {
        ((self.len >> 16) as u16 & 0x3FFF, self.len as u16)
    }
}

/// Writer for Lite registers.
pub struct RegisterWriterLite<'a> {
    register: &'a mut VirtualRegister,
}

impl<'a> RegisterWriter for RegisterWriterLite<'a> {
    type CtrlBlock = ControlBlockBuilderLite;

    unsafe fn ctrl_blk_addr(&mut self, block: &Self::CtrlBlock, addr: &AlignedAddressPair) {
        println!(
            "Writing control block to virt {:#010X} phys {:010X}",
            addr.virt_addr as usize, addr.phys_addr.addr as usize
        );
        block.write(addr.virt_addr);
        self.register
            .base_addr
            .offset(REG_CTRL_BLK_ADDR)
            .write_volatile(addr.phys_addr.addr as u32);
    }

    fn ctrl_status(&mut self) -> ControlStatusUpdate {
        ControlStatusUpdate::new(self.register)
    }

    unsafe fn next_ctrl_blk_addr(&mut self, block: &Self::CtrlBlock, addr: &AlignedAddressPair) {
        block.write(addr.virt_addr);
        self.register
            .base_addr
            .offset(REG_NEXT_CTRL_BLK_ADDR)
            .write_volatile(addr.phys_addr.addr as u32);
    }

    fn debug(&mut self) {
        // TODO
        unimplemented!()
    }
}

/// Writer for Normal registers.
pub struct RegisterWriterNormal<'a> {
    lite: RegisterWriterLite<'a>,
}

// TODO write full-only stuff

impl<'a> RegisterWriter for RegisterWriterNormal<'a> {
    type CtrlBlock = ControlBlockBuilderNormal;

    fn ctrl_status(&mut self) -> ControlStatusUpdate {
        self.lite.ctrl_status()
    }

    unsafe fn ctrl_blk_addr(&mut self, block: &Self::CtrlBlock, addr: &AlignedAddressPair) {
        self.lite.ctrl_blk_addr(&block.lite, addr)
    }

    unsafe fn next_ctrl_blk_addr(&mut self, block: &Self::CtrlBlock, addr: &AlignedAddressPair) {
        self.lite.next_ctrl_blk_addr(&block.lite, addr)
    }

    fn debug(&mut self) {
        self.lite.debug()
    }
}

/// A read only view of a DMA channel's registers.
pub struct RegisterViewLite<'a> {
    register: &'a VirtualRegister,
}

impl<'a> RegisterView for RegisterViewLite<'a> {
    type TxInfo = TransferInfoLite;
    type TxLen = u16;

    unsafe fn ctrl_status(&self) -> ControlStatus {
        let ptr = self.register.base_addr.offset(REG_CTRL_STATUS);
        let data = ptr.read_volatile();
        println!("Read CS: {:#010X} from {:#010X}", data, ptr as usize);
        ControlStatus::new(data)
    }

    unsafe fn ctrl_blk_addr(&self) -> Option<PhysicalAddr> {
        PhysicalAddr::with_ptr(self.register
            .base_addr
            .offset(REG_CTRL_BLK_ADDR)
            .read_volatile() as *mut u8)
    }

    unsafe fn tx_info(&self) -> TransferInfoLite {
        TransferInfoLite::new(self.register.base_addr.offset(REG_TX_INFO).read_volatile())
    }

    unsafe fn src_addr(&self) -> Option<PhysicalAddr> {
        PhysicalAddr::with_ptr(self.register.base_addr.offset(REG_SRC_ADDR).read_volatile() as *mut u8)
    }

    unsafe fn dst_addr(&self) -> Option<PhysicalAddr> {
        PhysicalAddr::with_ptr(self.register.base_addr.offset(REG_DST_ADDR).read_volatile() as *mut u8)
    }

    unsafe fn tx_len(&self) -> Self::TxLen {
        self.register.base_addr.offset(REG_TX_LEN).read_volatile() as u16
    }

    unsafe fn next_ctrl_blk_addr(&self) -> Option<PhysicalAddr> {
        PhysicalAddr::with_ptr(self.register
            .base_addr
            .offset(REG_NEXT_CTRL_BLK_ADDR)
            .read_volatile() as *mut u8)
    }

    unsafe fn debug(&self) -> Debug {
        let data = self.register.base_addr.offset(REG_DEBUG).read_volatile();
        println!("Read debug: {:#010X}", data);
        Debug { data }
    }
}

/// Register view for Normal channels.
pub struct RegisterViewNormal<'a> {
    lite: RegisterViewLite<'a>,
}

impl<'a> RegisterViewNormal<'a> {
    /// If in 2d mode, this is the (src stride, dest stride) added to the end of each row.
    fn stride(&self) -> (i16, i16) {
        let data = unsafe {
            self.lite
                .register
                .base_addr
                .offset(REG_STRIDE)
                .read_volatile()
        };
        (data as i16, (data >> 16) as i16)
    }
}

impl<'a> RegisterView for RegisterViewNormal<'a> {
    type TxInfo = TransferInfoNormal;
    type TxLen = TransferLength;

    unsafe fn ctrl_status(&self) -> ControlStatus {
        self.lite.ctrl_status()
    }

    unsafe fn ctrl_blk_addr(&self) -> Option<PhysicalAddr> {
        self.lite.ctrl_blk_addr()
    }

    unsafe fn tx_info(&self) -> Self::TxInfo {
        TransferInfoNormal::new(
            self.lite
                .register
                .base_addr
                .offset(REG_TX_INFO)
                .read_volatile(),
        )
    }

    unsafe fn src_addr(&self) -> Option<PhysicalAddr> {
        self.lite.src_addr()
    }

    unsafe fn dst_addr(&self) -> Option<PhysicalAddr> {
        self.lite.dst_addr()
    }

    unsafe fn tx_len(&self) -> Self::TxLen {
        let bits = self.lite
            .register
            .base_addr
            .offset(REG_TX_LEN)
            .read_volatile();
        // ignore top 2 bits
        let bits = bits & (!0 >> 2);
        TransferLength::with_length(bits)
    }

    unsafe fn next_ctrl_blk_addr(&self) -> Option<PhysicalAddr> {
        self.lite.next_ctrl_blk_addr()
    }

    unsafe fn debug(&self) -> Debug {
        self.lite.debug()
    }
}

/// Read-only snapshot of debug bits for a channel.
pub struct Debug {
    data: u32,
}

impl Debug {
    /// True if the channel is lite
    pub fn lite(&self) -> bool {
        self.bit(28)
    }

    /// 3 bit DMA version
    pub fn version(&self) -> u8 {
        ((self.data >> 25) & 0x7) as u8
    }

    /// 9 bit DMA state machine state
    pub fn state(&self) -> u16 {
        ((self.data >> 16) & 0x01FF) as u16
    }

    /// 8 bit DMA AXI ID
    pub fn axi_id(&self) -> u8 {
        (self.data >> 8) as u8
    }

    /// Number of outstanding writes
    pub fn outstanding_writes(&self) -> u8 {
        ((self.data >> 4) & 0x0F) as u8
    }

    /// Error on read response bus
    pub fn read_error(&self) -> bool {
        self.bit(2)
    }

    /// Error on (optional) read fifo
    pub fn fifo_error(&self) -> bool {
        self.bit(1)
    }

    /// Expected an AXI read last signal, but did not find it
    pub fn read_last_not_set_error(&self) -> bool {
        self.bit(0)
    }

    fn bit(&self, index: u8) -> bool {
        is_bit_set(self.data, index)
    }
}

/// View helpers for the Transfer Info registers.

use super::control_block::*;
use super::is_bit_set;

/// Transfer Info register contents for Lite channels.
///
/// See `*_TI` registers.
pub struct TransferInfoLite {
    data: u32,
}

impl TransferInfoLite {
    pub(crate) fn new(data: u32) -> TransferInfoLite {
        TransferInfoLite { data }
    }

    /// 5-bit number of wait cycles after each read or write.
    pub fn wait_cycles(&self) -> u8 {
        ((self.data >> TI_WAIT_CYCLES) & 0x1F) as u8
    }

    /// 5-bit peripheral number whose ready and panic signals are being used. Use 0 for an un-paced
    /// transfer.
    pub fn peripheral_map(&self) -> u8 {
        ((self.data >> TI_PERIPHERAL_MAP) & 0x1F) as u8
    }

    /// 4-bit burst length in words. 0 will result in a single transfer.
    pub fn burst_length(&self) -> u8 {
        ((self.data >> TI_BURST_TX_LEN) & 0x0F) as u8
    }

    /// Using DREQ for the selected peripheral to gate reads.
    pub fn read_dreq(&self) -> bool {
        self.bit(TI_SRC_DREQ)
    }

    /// If true, using 128-bit reads, else 32-bit reads
    pub fn read_wide(&self) -> bool {
        self.bit(TI_SRC_WIDTH)
    }

    /// If true, source is incremented (see `read_wide()`), else address does not change
    pub fn src_increment(&self) -> bool {
        self.bit(TI_SRC_INC)
    }

    /// Using DREQ for the selected peripheral to gate writes.
    pub fn write_dreq(&self) -> bool {
        self.bit(TI_DST_DREQ)
    }

    /// If true, using 128-bit reads, else 32-bit reads
    pub fn write_wide(&self) -> bool {
        self.bit(TI_DST_WIDTH)
    }

    /// If true, destination is incremented (see `write_wide()`), else address does not change
    pub fn dst_increment(&self) -> bool {
        self.bit(TI_DST_INC)
    }

    /// If true, waits for AXI write response for each write before continuing
    pub fn write_wait(&self) -> bool {
        self.bit(TI_WAIT_RESP)
    }

    /// If true, generate an interrupt when control block completes
    pub fn interrupt_when_complete(&self) -> bool {
        self.bit(TI_INTERRUPT)
    }

    fn bit(&self, index: u8) -> bool {
        is_bit_set(self.data, index)
    }
}

/// Transfer Info register contents for Normal channels.
///
/// See `*_TI` registers.
pub struct TransferInfoNormal {
    lite: TransferInfoLite,
}

impl TransferInfoNormal {
    pub(crate) fn new(data: u32) -> TransferInfoNormal {
        TransferInfoNormal {
            lite: TransferInfoLite::new(data),
        }
    }
    /// Don't use AXI bursts.
    pub fn no_wide_bursts(&self) -> bool {
        self.lite.bit(TI_NO_WIDE_BURSTS)
    }

    /// If true, won't actually read; just writes zero.
    pub fn src_ignore(&self) -> bool {
        self.lite.bit(TI_SRC_IGNORE)
    }

    /// If true, won't actually write
    pub fn dst_ignore(&self) -> bool {
        self.lite.bit(TI_DST_IGNORE)
    }

    /// If true, transfer length is interpreted as 2d mode w/ stride
    pub fn tx_2d_mode(&self) -> bool {
        self.lite.bit(TI_2D_MODE)
    }
}

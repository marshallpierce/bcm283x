/// View and update Control Status.

use super::*;

const CS_RESET: u8 = 31;
const CS_ABORT: u8 = 30;
const CS_DISABLE_DEBUG: u8 = 29;
const CS_WAIT_FOR_OUTSTANDING_WRITES: u8 = 28;
const CS_PANIC_PRIORITY: u8 = 20;
const CS_PRIORITY: u8 = 16;
const CS_ERROR: u8 = 8;
const CS_WAITING_FOR_OUTSTANDING_WRITES: u8 = 6;
const CS_DREQ_STOPS_DMA: u8 = 5;
const CS_PAUSED: u8 = 4;
const CS_DREQ: u8 = 3;
const CS_INTERRUPT: u8 = 2;
const CS_END: u8 = 1;
const CS_ACTIVE: u8 = 0;

/// Read-only snapshot of control status for a DMA channel.
///
/// Represents the *_CS registers; see 4.2.1.2 in peripheral doc.
pub struct ControlStatus {
    data: u32,
}

impl ControlStatus {
    pub(crate) fn new(data: u32) -> ControlStatus {
        ControlStatus { data }
    }

    pub fn disable_debug(&self) -> bool {
        self.bit(CS_DISABLE_DEBUG)
    }

    pub fn wait_for_outstanding_writes(&self) -> bool {
        self.bit(CS_WAIT_FOR_OUTSTANDING_WRITES)
    }

    /// Returns a 4-bit value.
    pub fn panic_priority(&self) -> u8 {
        (self.data >> CS_PANIC_PRIORITY & 0x0F) as u8
    }

    /// Returns a 4-bit value.
    pub fn priority(&self) -> u8 {
        (self.data >> CS_PRIORITY & 0x0F) as u8
    }

    pub fn error(&self) -> bool {
        self.bit(CS_ERROR)
    }

    pub fn waiting_for_outstanding_writes(&self) -> bool {
        self.bit(CS_WAITING_FOR_OUTSTANDING_WRITES)
    }

    pub fn paused_by_dreq(&self) -> bool {
        self.bit(CS_DREQ_STOPS_DMA)
    }

    pub fn paused(&self) -> bool {
        self.bit(CS_PAUSED)
    }

    pub fn dreq_requesting_data(&self) -> bool {
        self.bit(CS_DREQ)
    }

    pub fn interrupted(&self) -> bool {
        self.bit(CS_INTERRUPT)
    }

    pub fn ended(&self) -> bool {
        self.bit(CS_END)
    }

    pub fn active(&self) -> bool {
        self.bit(CS_ACTIVE)
    }

    #[inline]
    fn bit(&self, index: u8) -> bool {
        is_bit_set(self.data, index)
    }
}

/// Write to control status register for a DMA channel.
///
/// All bits start unset.
///
/// To use, stage any changes you want to make by calling the appropriate functions
/// (`clear_interrupt()`, etc), then call `write()`.
///
/// Represents the *_CS registers; see 4.2.1.2 in peripheral doc.
pub struct ControlStatusUpdate<'a> {
    data: u32,
    register: &'a mut VirtualRegister,
}

impl<'a> ControlStatusUpdate<'a> {
    pub(crate) fn new(register: &mut VirtualRegister) -> ControlStatusUpdate {
        ControlStatusUpdate { data: 0, register }
    }

    /// Reset the DMA. This does not reset this struct; it sets the reset bit.
    ///
    /// Self clearing.
    pub fn reset(&mut self, state: bool) -> &'a mut ControlStatusUpdate {
        self.set(CS_RESET, state);
        self
    }

    /// Abort current control block and go to the next one.
    ///
    /// Self clearing.
    pub fn abort(&mut self, state: bool) -> &'a mut ControlStatusUpdate {
        self.set(CS_ABORT, state);
        self
    }

    /// Don't stop when debug pause signal is asserted.
    pub fn disable_debug(&mut self, state: bool) -> &'a mut ControlStatusUpdate {
        self.set(CS_DISABLE_DEBUG, state);
        self
    }

    /// Wait for outstanding writes before loading the next CB, etc.
    pub fn wait_for_outstanding_writes(&mut self, state: bool) -> &'a mut ControlStatusUpdate {
        self.set(CS_WAIT_FOR_OUTSTANDING_WRITES, state);
        self
    }

    /// Set priority of panicking AXI bus transactions. Used when the panic bit is 1.
    ///
    /// `priority` is a 4-bit value, with 0 being the lowest priority.
    pub fn panic_priority(&mut self, priority: u8) -> &'a mut ControlStatusUpdate {
        self.data = set_bits(self.data, CS_PANIC_PRIORITY, priority, 4);
        self
    }

    /// Set normal priority. Used when the panic bit is 0.
    ///
    /// `priority` is a 4-bit value, with 0 being the lowest priority.
    pub fn priority(&mut self, priority: u8) -> &'a mut ControlStatusUpdate {
        self.data = set_bits(self.data, CS_PRIORITY, priority, 4);
        self
    }

    /// Clear interrupt flag.
    pub fn clear_interrupt(&mut self, state: bool) -> &'a mut ControlStatusUpdate {
        self.set(CS_INTERRUPT, state);
        self
    }

    /// Clear the DMA End flag (&mut self, set when the transfer is complete).
    pub fn clear_dma_end(&mut self, state: bool) -> &'a mut ControlStatusUpdate {
        self.set(CS_END, state);
        self
    }

    /// Activate/deactivate the channel.
    pub fn activate(&mut self, state: bool) -> &'a mut ControlStatusUpdate {
        self.set(CS_ACTIVE, state);
        self
    }

    /// Write the state to the control status register. All bits in the register are written to, so
    /// previous settings will be entirely overwritten.
    pub unsafe fn write_register(&self) {
        let ptr = self.register.base_addr.offset(REG_CTRL_STATUS);
        println!("Writing CS {:#010X} to {:#010X}", self.data, ptr as usize);
        ptr.write_volatile(self.data)
    }

    #[inline]
    fn set(&mut self, offset: u8, state: bool) {
        self.data = set_bit(self.data, offset, state)
    }
}

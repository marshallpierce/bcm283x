//! Configure the DMA engine.
//!
//! There are 16 DMA channels. Some of them are "Lite" and the others are "Normal". The Lite
//! channels don't support as many features and have less bandwidth.
//!
//! To prevent you from configuring features that won't work in practice, there are separate types
//! for Lite and Normal channels that precisely express the available features in each case. Since
//! Lite features are a strict subset, you can treat all channels as Lite if you don't need Full
//! features. See section 4.5 of the peripheral documentation for more.
mod control_status;
mod control_block;
mod register;
mod tx_info;

pub mod linux;

use super::linux::*;
use self::register::*;

pub use self::control_block::{ControlBlockBuilder, ControlBlockBuilderLite,
                              ControlBlockBuilderNormal, TransferInfoBuilderLite,
                              TransferInfoBuilderNormal};
pub use self::register::{RegisterSet, RegisterSetLite, RegisterSetNormal, RegisterView,
                         RegisterViewLite, RegisterViewNormal, RegisterWriter, RegisterWriterLite,
                         RegisterWriterNormal};

/// All channels in the system.
///
/// Index into this to get a reference to the channel you want.
pub const CHANNELS: [DmaChannel; 16] = [
    DmaChannel {
        id: 0,
        group: DmaRegisterGroup::Primary,
    },
    DmaChannel {
        id: 1,
        group: DmaRegisterGroup::Primary,
    },
    DmaChannel {
        id: 2,
        group: DmaRegisterGroup::Primary,
    },
    DmaChannel {
        id: 3,
        group: DmaRegisterGroup::Primary,
    },
    DmaChannel {
        id: 4,
        group: DmaRegisterGroup::Primary,
    },
    DmaChannel {
        id: 5,
        group: DmaRegisterGroup::Primary,
    },
    DmaChannel {
        id: 6,
        group: DmaRegisterGroup::Primary,
    },
    DmaChannel {
        id: 7,
        group: DmaRegisterGroup::Primary,
    },
    DmaChannel {
        id: 8,
        group: DmaRegisterGroup::Primary,
    },
    DmaChannel {
        id: 9,
        group: DmaRegisterGroup::Primary,
    },
    DmaChannel {
        id: 10,
        group: DmaRegisterGroup::Primary,
    },
    DmaChannel {
        id: 11,
        group: DmaRegisterGroup::Primary,
    },
    DmaChannel {
        id: 12,
        group: DmaRegisterGroup::Primary,
    },
    DmaChannel {
        id: 13,
        group: DmaRegisterGroup::Primary,
    },
    DmaChannel {
        id: 14,
        group: DmaRegisterGroup::Primary,
    },
    DmaChannel {
        id: 15,
        group: DmaRegisterGroup::Secondary,
    },
];

/// Offset from primary group's base physical address. See peripherals doc p47.
const GLOBAL_INTERRUPT_STATUS_OFFSET: isize = 0xFE0;

/// Offset from primary group's base physical address. See peripherals doc p47.
const GLOBAL_ENABLE_OFFSET: isize = 0xFF0;

/// Physical address offset in bytes from `dma_base()` of start of register block for a DMA channel.
///
/// Offset is always non-negative and <= 0xe00.
fn dma_chan_offset(chan: &DmaChannel) -> isize {
    match chan.group {
        DmaRegisterGroup::Primary => chan.id as isize * 0x100,
        DmaRegisterGroup::Secondary => 0,
    }
}

/// DMA registers are arranged in two blocks which span different pages.
fn dma_base(peripheral: &PeripheralAddr, group: DmaRegisterGroup) -> u32 {
    match group {
        DmaRegisterGroup::Primary => peripheral.phys_base_addr + 0x007000,
        DmaRegisterGroup::Secondary => peripheral.phys_base_addr + 0xE05000,
    }
}

/// An identifier for an individual channel.
#[derive(Debug)]
pub struct DmaChannel {
    /// DMA channel number `[0-15]`
    id: u8,
    group: DmaRegisterGroup,
}

impl DmaChannel {
    /// This doesn't query the DMA channel debug registers; it just returns what the peripheral
    /// documentation says.
    ///
    /// Note that channel 15 seems like it should be lite because its id is > 6, and its registers
    /// are not listed in the full-only register sets like *_STRIDE, but its registers aren't listed
    /// in the universal register sets (e.g. *_CS) either, so it seems to have been somewhat
    /// forgotten.
    fn is_lite(&self) -> bool {
        self.id > 6 && self.id != 15 // TODO 15 ???
    }

    /// Channel id, in `[0, 15]`.
    pub fn id(&self) -> u8 {
        self.id
    }
}

/// Designator for which group of registers is used for a particular channel.
/// See peripherals doc section 4.2
#[derive(Clone, Copy, Eq, PartialEq, Debug)]
enum DmaRegisterGroup {
    Primary,
    Secondary,
}

/// Read-only snapshot of global interrupt register.
///
/// See the INT_STATUS register.
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub struct GlobalInterruptStatus {
    data: u32,
}

impl GlobalInterruptStatus {
    pub fn is_interrupted(&self, channel: &DmaChannel) -> bool {
        is_bit_set(self.data, channel.id)
    }
}

/// Read-only snapshot of global enable register.
///
/// See the ENABLE register.
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub struct GlobalEnabledStatus {
    data: u32,
}

impl GlobalEnabledStatus {
    pub fn is_enabled(&self, channel: &DmaChannel) -> bool {
        // TODO what to da about channel 15? not listed in docs
        is_bit_set(self.data, channel.id)
    }
}

#[inline]
fn is_bit_set(data: u32, index: u8) -> bool {
    (data & (1 << index)) > 0
}

/// Set a single bit to 1 if state = true and vice versa.
#[inline]
fn set_bit(data: u32, offset: u8, state: bool) -> u32 {
    if state {
        data | 1 << offset
    } else {
        data & !(1 << offset)
    }
}

/// Set the low `num_bits` bits of `input` in `data` at offset `offset`.
///
/// Panics if `input` has any bits higher than `num_bits` set.
#[inline]
fn set_bits(mut data: u32, offset: u8, input: u8, num_bits: u8) -> u32 {
    let max_value = (1_u32 << num_bits) - 1;
    assert!(input as u32 <= max_value, "input had too many bits");
    assert!(offset + num_bits <= 32);
    // clear existing
    data &= !(max_value << offset);
    data | (input as u32) << offset
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    #[should_panic]
    fn set_bits_panics_too_many_bits() {
        set_bits(0, 3, 0xFF, 7);
    }

    #[test]
    fn set_bits_clears_existing_bits() {
        assert_eq!(0xFE67, set_bits(0xFFFF, 3, 0b1100, 6))
    }

    #[test]
    fn set_bits_low_bits() {
        assert_eq!(0xFFF9, set_bits(0xFFFF, 0, 0b1001, 4))
    }

    #[test]
    fn set_bits_hi_bits() {
        assert_eq!(0x9FFF0000, set_bits(0xFFFF0000, 28, 0b1001, 4))
    }
}

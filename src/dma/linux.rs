//! Access the DMA registers on Linux.
//!
//! When running in an OS, naturally you don't have direct access to physical memory, but you can
//! `mmap()` `/dev/mem` to sneak around behind the kernel's back. This requires that you be `root`.
extern crate libc;
extern crate rpi_info;
extern crate vm_info;

use super::*;

use std::{fs, ptr};
use std::os::unix::fs::OpenOptionsExt;
use std::os::unix::io::AsRawFd;
use std::convert::TryFrom;

/// Errors that can happen when initializing DMA pages.
#[derive(Debug, PartialEq, Eq)]
pub enum DmaInitError {
    /// Could not figure out what kind of Raspberry Pi is in use
    UnknownHardware,
    /// Could not access /dev/mem
    DevMem,
    /// Could not mmap dma registers
    Mmap,
}

/// `mmap()`-based access to DMA registers.
pub struct DmaControlRegisters {
    /// Virtual address of primary page of control registers
    primary_page: *mut u8,
    /// Secondary page (only channel 15)
    secondary_page: *mut u8,
}

impl DmaControlRegisters {
    /// Allocate memory to expose DMA registers.
    // TODO only allow one of these at a time
    pub unsafe fn new() -> Result<DmaControlRegisters, DmaInitError> {
        let processor = rpi_info::load_cpuinfo()
            .map_err(|_| DmaInitError::UnknownHardware)
            .and_then(|o| o.ok_or(DmaInitError::UnknownHardware))
            .map(|i| i.processor)?;

        let addrs = PeripheralAddr::with_processor(processor);

        let f = fs::OpenOptions::new()
            .read(true)
            .write(true)
            .custom_flags(libc::O_SYNC)
            .open("/dev/mem")
            .map_err(|_| DmaInitError::DevMem)?;

        // allocate pages for both register groups
        let primary_page =
            mmap_dma_register_group(f.as_raw_fd(), &addrs, DmaRegisterGroup::Primary)?;
        let secondary_page =
            mmap_dma_register_group(f.as_raw_fd(), &addrs, DmaRegisterGroup::Secondary)?;

        Ok(DmaControlRegisters {
            primary_page,
            secondary_page,
        })
    }

    /// Access a channel as if it's a Lite channel.
    ///
    /// This will work on all channels; it just won't let you access Normal-only features.
    pub unsafe fn channel_lite(&self, channel: &DmaChannel) -> RegisterSetLite {
        RegisterSetLite {
            register: self.register(channel),
        }
    }

    /// Access a channel as if it's a Normal channel.
    ///
    /// This will panic if it's given a Lite-only channel.
    pub unsafe fn channel_normal(&self, channel: &DmaChannel) -> RegisterSetNormal {
        if channel.is_lite() {
            panic!("Channel id {} is lite", channel.id);
        }

        RegisterSetNormal {
            register: self.register(channel),
        }
    }

    /// Get the current global enable status for all channels.
    ///
    /// Represents the ENABLE register.
    pub unsafe fn global_enable_status(&self) -> GlobalEnabledStatus {
        let enabled_ptr = self.primary_page.offset(GLOBAL_ENABLE_OFFSET);
        let data = (enabled_ptr as *const u32).read_volatile();
        println!(
            "ENABLE ptr: {:#010X} data: {:#010X} {:#034b}",
            enabled_ptr as usize, data, data
        );
        GlobalEnabledStatus { data }
    }

    /// Get the current global interrupt status for all channels.
    ///
    /// Represents the INT_STATUS register.
    pub unsafe fn global_interrupt_status(&self) -> GlobalInterruptStatus {
        let int_status_ptr = self.primary_page.offset(GLOBAL_INTERRUPT_STATUS_OFFSET);
        let data = (int_status_ptr as *const u32).read_volatile();
        println!(
            "INT_STATUS ptr: {:#010X} data: {:#010X} {:#034b}",
            int_status_ptr as usize, data, data
        );
        GlobalInterruptStatus { data }
    }

    unsafe fn register(&self, channel: &DmaChannel) -> VirtualRegister {
        let base = match channel.group {
            DmaRegisterGroup::Primary => self.primary_page,
            DmaRegisterGroup::Secondary => self.secondary_page,
        };
        let reg_base = base.offset(dma_chan_offset(channel));
        println!(
            "Channel {} virtual reg base: {:#010X}",
            channel.id, reg_base as usize
        );

        VirtualRegister {
            base_addr: reg_base as *mut u32,
        }
    }
}

impl Drop for DmaControlRegisters {
    fn drop(&mut self) {
        unsafe {
            let primary_res = libc::munmap(self.primary_page as *mut libc::c_void, *PAGE_SIZE);
            let secondary_res = libc::munmap(self.secondary_page as *mut libc::c_void, *PAGE_SIZE);
            assert_eq!(0, primary_res, "munmap failed");
            assert_eq!(0, secondary_res, "munmap failed");
        }
    }
}

unsafe fn mmap_dma_register_group(
    fd: libc::c_int,
    addrs: &PeripheralAddr,
    group: DmaRegisterGroup,
) -> Result<*mut u8, DmaInitError> {
    let dma_base = super::dma_base(addrs, group);
    let ret = libc::mmap(
        ptr::null_mut(),
        *PAGE_SIZE,
        libc::PROT_READ | libc::PROT_WRITE,
        libc::MAP_SHARED,
        fd,
        libc::off_t::try_from(dma_base).expect("could not express DMA register base as an offset"),
    );

    println!(
        "{:?} dma register page physical addr {:#010X} virt addr {:#010X}",
        group, dma_base, ret as usize
    );

    return if ret == libc::MAP_FAILED {
        Err(DmaInitError::Mmap)
    } else {
        Ok(ret as *mut u8)
    };
}

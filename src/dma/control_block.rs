//! Build Control Blocks.

use super::*;

use std::borrow::Borrow;

// offsets
pub(crate) const TI_NO_WIDE_BURSTS: u8 = 26;
pub(crate) const TI_WAIT_CYCLES: u8 = 21;
pub(crate) const TI_PERIPHERAL_MAP: u8 = 16;
pub(crate) const TI_BURST_TX_LEN: u8 = 12;
pub(crate) const TI_SRC_IGNORE: u8 = 11;
pub(crate) const TI_SRC_DREQ: u8 = 10;
pub(crate) const TI_SRC_WIDTH: u8 = 9;
pub(crate) const TI_SRC_INC: u8 = 8;
pub(crate) const TI_DST_IGNORE: u8 = 7;
pub(crate) const TI_DST_DREQ: u8 = 6;
pub(crate) const TI_DST_WIDTH: u8 = 5;
pub(crate) const TI_DST_INC: u8 = 4;
pub(crate) const TI_WAIT_RESP: u8 = 3;
pub(crate) const TI_2D_MODE: u8 = 1;
pub(crate) const TI_INTERRUPT: u8 = 0;

/// Once serialized into memory, the physical address can be set as the control block address in the
/// appropriate DMA channel's register to configure a transfer.
///
/// Setting at least src_addr and dst_addr is required for a Control Block to be at all useful,
/// so this will panic if you try to write a control block without at least setting those.
///
/// See peripheral doc section 4.2.1.1.
pub trait ControlBlockBuilder {
    type TransferInfo: Sized;
    type TxLen;

    /// Create a suitable TransferInfoBuilder implementation and pass it to this method.
    ///
    /// Corresponds to the `TI` section in a control block, which is then copied to the appropriate
    /// `*_TI` register upon configuring a channel.
    fn tx_info(&mut self, tx_info: Self::TransferInfo) -> &mut Self;

    /// Physical address of transfer source.
    ///
    /// Corresponds to the `DEST_AD` section in a control block, which is then copied to the
    /// appropriate `*_DEST_AD` register upon configuring a channel.
    fn src_addr(&mut self, addr: PhysicalAddr) -> &mut Self;

    /// Physical address of transfer destination.
    ///
    /// Corresponds to the `DEST_AD` section in a control block, which is then copied to the
    /// appropriate `*_DEST_AD` register upon configuring a channel.
    fn dst_addr(&mut self, addr: PhysicalAddr) -> &mut Self;

    /// On a Normal channel, this can be either a 1D transfer length or a 2D stride transfer. On a
    /// Lite channel, only 1D transfers are supported.
    ///
    /// Corresponds to the `TXFR_LEN` section in a control block, which is then copied to the
    /// appropriate `*_TXFR_LEN` register upon configuring a channel.
    fn tx_len(&mut self, len: Self::TxLen) -> &mut Self;

    /// Either the next control block physical address or `None` to indicate DMA should stop after
    /// this block.
    ///
    /// Corresponds to the `NEXTCONBK` section in a control block, which is then copied to the
    /// appropriate `*_NEXTCONBK` register upon configuring a channel.
    // TODO take a block and aligned address together?
    fn next_ctrl_blk_addr(&mut self, addr: Option<PhysicalAddr>) -> &mut Self;
}

/// Builder for Control Blocks for Lite channels.
#[derive(Debug)]
pub struct ControlBlockBuilderLite {
    tx_info: u32,
    src_addr: Option<PhysicalAddr>,
    dst_addr: Option<PhysicalAddr>,
    tx_len: u32,
    stride: u32,
    next_ctrl_blk_addr: Option<PhysicalAddr>,
}

impl ControlBlockBuilderLite {
    pub fn new() -> ControlBlockBuilderLite {
        ControlBlockBuilderLite {
            tx_info: 0,
            src_addr: None,
            dst_addr: None,
            tx_len: 0,
            stride: 0,
            next_ctrl_blk_addr: None,
        }
    }

    pub(crate) unsafe fn write(&self, virt_ptr: *mut u8) {
        assert_eq!(
            0,
            (virt_ptr as usize) & 0xFF,
            "Control block address must be 256-bit aligned"
        );

        let cb_ptr = virt_ptr as *mut u32;
        println!("Writing CB to virt {:#010X}", cb_ptr as usize);

        println!("\tTX info {:#010X}", self.tx_info);
        cb_ptr.write_volatile(self.tx_info);

        let src_addr_phys = self.src_addr.expect("Must set src_addr").addr as u32;
        println!("\tSrc addr phys {:#010X}", src_addr_phys);
        cb_ptr.offset(1).write_volatile(src_addr_phys);

        let dst_addr_phys = self.dst_addr.expect("Must set dst_addr").addr as u32;
        println!("\tDst addr phys {:#010X}", dst_addr_phys);
        cb_ptr.offset(2).write_volatile(dst_addr_phys);

        println!("\tTX len {:#010X}", self.tx_len);
        cb_ptr.offset(3).write_volatile(self.tx_len);

        println!("\tStride {:#010X}", self.stride);
        cb_ptr.offset(4).write_volatile(self.stride);

        let next_addr = self.next_ctrl_blk_addr.map(|p| p.addr as u32).unwrap_or(0);
        println!("\tNext CB addr phys {:#010X}", next_addr);
        cb_ptr.offset(5).write_volatile(next_addr);

        // remaining 64 bits must be set to zero
        cb_ptr.offset(6).write_volatile(0);
        cb_ptr.offset(7).write_volatile(0);
    }
}

impl ControlBlockBuilder for ControlBlockBuilderLite {
    type TransferInfo = TransferInfoBuilderLite;
    type TxLen = u16;

    fn tx_info(&mut self, tx_info: Self::TransferInfo) -> &mut Self {
        self.tx_info = tx_info.data;
        self
    }

    fn src_addr(&mut self, addr: PhysicalAddr) -> &mut Self {
        self.src_addr = Some(addr);
        self
    }

    fn dst_addr(&mut self, addr: PhysicalAddr) -> &mut Self {
        self.dst_addr = Some(addr);
        self
    }

    fn tx_len(&mut self, len: Self::TxLen) -> &mut Self {
        self.tx_len = len as u32;
        self
    }

    fn next_ctrl_blk_addr(&mut self, addr: Option<PhysicalAddr>) -> &mut Self {
        self.next_ctrl_blk_addr = addr;
        self
    }
}

/// Builder for Control Blocks for Normal channels.
#[derive(Debug)]
pub struct ControlBlockBuilderNormal {
    pub(crate) lite: ControlBlockBuilderLite,
}

impl ControlBlockBuilderNormal {
    pub fn new() -> Self {
        ControlBlockBuilderNormal {
            lite: ControlBlockBuilderLite::new(),
        }
    }

    pub(crate) unsafe fn write(&self, virt_ptr: *mut u8) {
        self.lite.write(virt_ptr)
    }

    /// Set the source and destination stride to be applied to the source and destination addresses
    /// respectively after each row.
    ///
    /// Only applicable if the Transfer Info is set to use 2D mode.
    pub fn stride(&mut self, src_stride: i16, dst_stride: i16) -> &mut Self {
        self.lite.stride = ((dst_stride as u32) << 16) | src_stride as u32;
        self
    }
}

impl ControlBlockBuilder for ControlBlockBuilderNormal {
    type TransferInfo = TransferInfoBuilderNormal;
    type TxLen = TransferLength;

    fn tx_info(&mut self, tx_info: Self::TransferInfo) -> &mut Self {
        self.lite.tx_info = tx_info.borrow().lite.data;
        self
    }

    fn src_addr(&mut self, addr: PhysicalAddr) -> &mut Self {
        self.lite.src_addr(addr);
        self
    }
    fn dst_addr(&mut self, addr: PhysicalAddr) -> &mut Self {
        self.lite.dst_addr(addr);
        self
    }

    fn tx_len(&mut self, len: Self::TxLen) -> &mut Self {
        self.lite.tx_len = len.len;
        self
    }

    fn next_ctrl_blk_addr(&mut self, addr: Option<PhysicalAddr>) -> &mut Self {
        self.lite.next_ctrl_blk_addr(addr);
        self
    }
}

/// Build Transfer Info for a Control Block for a Lite channel.
///
/// Transfer Info is read-only once it's in the `*_TI` registers, so this is only used for initial
/// configuration, not updating registers.
#[derive(Debug, Clone)]
pub struct TransferInfoBuilderLite {
    data: u32,
}

impl TransferInfoBuilderLite {
    pub fn new() -> Self {
        TransferInfoBuilderLite { data: 0 }
    }

    // TODO make trait

    /// 5-bit number of wait cycles after each read or write.
    pub fn wait_cycles(&mut self, cycles: u8) -> &mut Self {
        self.data = set_bits(self.data, TI_WAIT_CYCLES, cycles, 5);
        self
    }

    /// 5-bit peripheral number whose ready and panic signals are being used. Use 0 for an un-paced
    /// transfer.
    pub fn peripheral_map(&mut self, peripheral: u8) -> &mut Self {
        self.data = set_bits(self.data, TI_PERIPHERAL_MAP, peripheral, 5);
        self
    }

    /// 4-bit burst length in words. 0 will result in a single transfer.
    pub fn burst_length(&mut self, burst_length: u8) -> &mut Self {
        self.data = set_bits(self.data, TI_BURST_TX_LEN, burst_length, 4);
        self
    }

    /// Using DREQ for the selected peripheral to gate reads.
    pub fn read_dreq(&mut self, state: bool) -> &mut Self {
        self.set(TI_SRC_DREQ, state);
        self
    }

    /// If true, using 128-bit reads, else 32-bit reads
    pub fn read_wide(&mut self, state: bool) -> &mut Self {
        self.set(TI_SRC_WIDTH, state);
        self
    }

    /// If true, source is incremented (see `read_wide()`), else address does not change
    pub fn src_increment(&mut self, state: bool) -> &mut Self {
        self.set(TI_SRC_INC, state);
        self
    }

    /// Using DREQ for the selected peripheral to gate writes.
    pub fn write_dreq(&mut self, state: bool) -> &mut Self {
        self.set(TI_DST_DREQ, state);
        self
    }

    /// If true, using 128-bit reads, else 32-bit reads
    pub fn write_wide(&mut self, state: bool) -> &mut Self {
        self.set(TI_DST_WIDTH, state);
        self
    }

    /// If true, destination is incremented (see `write_wide()`), else address does not change
    pub fn dst_increment(&mut self, state: bool) -> &mut Self {
        self.set(TI_DST_INC, state);
        self
    }

    /// If true, waits for AXI write response for each write before continuing
    pub fn write_wait(&mut self, state: bool) -> &mut Self {
        self.set(TI_WAIT_RESP, state);
        self
    }

    /// If true, generate an interrupt when control block completes
    pub fn interrupt_when_complete(&mut self, state: bool) -> &mut Self {
        self.set(TI_INTERRUPT, state);
        self
    }

    #[inline]
    fn set(&mut self, offset: u8, state: bool) {
        self.data = set_bit(self.data, offset, state)
    }
}

/// Build Transfer Info for a Control Block for a Normal channel.
///
/// See `TransferInfoBuilderLite`.
#[derive(Debug, Clone)]
pub struct TransferInfoBuilderNormal {
    lite: TransferInfoBuilderLite,
}

impl TransferInfoBuilderNormal {
    /// Don't use AXI bursts.
    pub fn no_wide_bursts(&mut self, state: bool) -> &mut Self {
        self.lite.set(TI_NO_WIDE_BURSTS, state);
        self
    }

    /// If true, won't actually read; just writes zero.
    pub fn src_ignore(&mut self, state: bool) -> &mut Self {
        self.lite.set(TI_SRC_IGNORE, state);
        self
    }

    /// If true, won't actually write
    pub fn dst_ignore(&mut self, state: bool) -> &mut Self {
        self.lite.set(TI_DST_IGNORE, state);
        self
    }

    /// If true, transfer length is interpreted as 2d mode w/ stride
    pub fn tx_2d_mode(&mut self, state: bool) -> &mut Self {
        self.lite.set(TI_2D_MODE, state);
        self
    }
}

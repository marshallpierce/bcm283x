//! Linux-specific support for memory wrangling.

extern crate libc;
extern crate rpi_info;
extern crate vm_info;

use std::{fmt, mem, ptr};

lazy_static! {
    pub(crate) static ref PAGE_SIZE: usize = vm_info::page_size().expect("Could not get page size");
}

pub(crate) struct PeripheralAddr {
    pub(crate) phys_base_addr: u32,
}

impl PeripheralAddr {
    pub(crate) fn with_processor(p: rpi_info::Processor) -> PeripheralAddr {
        PeripheralAddr {
            phys_base_addr: match p {
                rpi_info::Processor::BCM2835 => 0x20000000,
                rpi_info::Processor::BCM2836 | rpi_info::Processor::BCM2837 => 0x3F000000,
            },
        }
    }
}

/// A wrapper around a physical address.
#[derive(Clone, Copy, Eq, PartialEq)]
pub struct PhysicalAddr {
    pub(crate) addr: *mut u8,
}

impl PhysicalAddr {
    /// Returns `None` if the address is null.
    pub(crate) fn with_ptr(addr: *mut u8) -> Option<PhysicalAddr> {
        if addr.is_null() {
            None
        } else {
            Some(PhysicalAddr { addr })
        }
    }
}

impl fmt::Debug for PhysicalAddr {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "[PhysicalAddr {:#010X}]", self.addr as usize)
    }
}

/// A virtual address / physical address pair for some allocated memory.
///
/// Be extremely careful doing anything with this type other than some initial setup and then
/// handing it off to DMA, etc. Because Rust doesn't really know about the underlying memory, it
/// doesn't play by the rules.
///
/// If you're using this type it's probably because you're interacting with DMA. If so, it's
/// important to account for the fact that the DMA engine is NOT the CPU and does not have access
/// to the CPU's L1 cache, so you need to use volatile reads/writes when interacting with these
/// pointers.
pub struct AlignedAddressPair {
    // TODO abstract over mutability?
    pub(crate) phys_addr: PhysicalAddr,
    pub(crate) virt_addr: *mut u8,
    len: usize,
    // TODO Drop impl to deallocate? Box?
    // TODO volatile read/write?
}

impl AlignedAddressPair {
    /// virt_addr and phys_addr must be aligned to 256-bit boundaries.
    // TODO express page vs 256bit alignment requirements? I don't think anything actually needs
    // page alignment; it's just convenient because it also gives us 256-bit
    fn new(virt_addr: *mut u8, phys_addr: PhysicalAddr, len: usize) -> Self {
        assert_eq!(
            0,
            (virt_addr as usize) & 0xFF,
            "Virtual address must be 256-bit aligned"
        );
        assert_eq!(
            0,
            (phys_addr.addr as usize) & 0xFF,
            "Physical address must be 256-bit aligned"
        );
        Self {
            virt_addr,
            phys_addr,
            len,
        }
    }

    /// Returns the virtual address (i.e. the normal kind of memory address).
    ///
    /// Remember to use volatile reads/writes as appropriate.
    pub fn virt_addr(&self) -> *mut u8 {
        self.virt_addr
    }

    /// Returns the physical address.
    ///
    /// This is not to be used directly; hand it off to something like the DMA engine that expects
    /// physical addresses.
    pub fn phys_addr(&self) -> PhysicalAddr {
        self.phys_addr
    }

    /// Read from this address into the provided buffer.
    ///
    /// Panics if the length of the slice is longer than the length of this allocation.
    pub unsafe fn read_volatile(&self, buf: &mut [u8]) {
        assert!(buf.len() <= self.len);

        for i in 0..buf.len() {
            buf[i] = self.virt_addr.offset(i as isize).read_volatile();
        }
    }

    /// Write into this address from the provided buffer.
    ///
    /// Panics if the length of the slice is longer than the length of this allocation.
    pub unsafe fn write_volatile(&mut self, buf: &[u8]) {
        assert!(buf.len() <= self.len);

        for (index, &b) in buf.iter().enumerate() {
            self.virt_addr.offset(index as isize).write_volatile(b);
        }
    }
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub enum AllocError {
    /// Could not allocate
    AllocFailed,
    /// Could not `mlock()`
    LockFailed,
    /// Could not load the kernel's physical page info
    PhysPageMapUnreadable,
    /// The page map was there, but it didn't have page numbers in it. Are you root?
    PhysPageNumberUnavailable,
}

/// Allocate `pages` pages in memory.
///
/// The memory will be aligned to a page boundary, locked (not swappable), and zeroed.
pub fn alloc_pages(pages: usize) -> Result<AlignedAddressPair, AllocError> {
    assert!(pages > 0);
    let mut page_ptr = ptr::null_mut();
    let len = *PAGE_SIZE * pages;
    unsafe {
        if libc::posix_memalign(&mut page_ptr, *PAGE_SIZE, len) != 0 {
            return Err(AllocError::AllocFailed);
        }

        println!("Allocated ptr {:#016X}", page_ptr as usize);

        // make sure the page is actually there by locking
        if libc::mlock(page_ptr, len) != 0 {
            // TODO free memory
            return Err(AllocError::LockFailed);
        }

        // zero it for sanity
        ptr::write_bytes(page_ptr, 0x0, len / mem::size_of_val(&page_ptr));
    }

    let virt_addr = page_ptr as *mut u8;
    match phys_addr(virt_addr) {
        Ok(p) => Ok(AlignedAddressPair::new(
            virt_addr,
            PhysicalAddr { addr: p },
            len,
        )),
        Err(e) => {
            // TODO don't leak
            return Err(e);
        }
    }
}

/// Virt ptr must be aligned on page boundary
fn phys_addr(virt_ptr: *mut u8) -> Result<*mut u8, AllocError> {
    assert_eq!(virt_ptr as usize & 0xFFF, 0);
    let page = vm_info::page_map::read_page_map(
        vm_info::ProcessId::SelfPid,
        virt_ptr as usize / *PAGE_SIZE,
    ).map_err(|_| AllocError::PhysPageMapUnreadable)?;

    println!("page:");
    println!("\tPFN:\t{:?}", page.page_frame());
    println!(
        "\tPhys addr:\t{:#010X}",
        page.page_frame().unwrap_or(0) as usize * *PAGE_SIZE
    );
    println!("\tPresent:\t{:?}", page.is_present());

    page.page_frame()
        .map(|pfn| pfn as usize * *PAGE_SIZE)
        .map(|ptr| ptr as *mut u8)
        .ok_or(AllocError::PhysPageNumberUnavailable)
}

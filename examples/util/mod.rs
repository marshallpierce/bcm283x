extern crate bcm283x;
use bcm283x::dma::{self, RegisterSet, RegisterView};

pub unsafe fn print_channel(
    channel: &dma::DmaChannel,
    dma_pages: &dma::linux::DmaControlRegisters,
    global_enabled: dma::GlobalEnabledStatus,
    global_interrupt: dma::GlobalInterruptStatus,
) {
    println!(
        "\n--------------------------------------\nChannel {}",
        channel.id()
    );
    let reg = dma_pages.channel_lite(channel);

    println!("\t* Global bits");
    println!("\tEnabled: {}", global_enabled.is_enabled(channel));
    println!(
        "\tInterrupted: {}",
        global_interrupt.is_interrupted(channel)
    );

    println!("\n\t* Status register");
    let status = reg.view().ctrl_status();
    println!("\tPriority: {}", status.priority());
    println!("\tError: {}", status.error());
    println!("\tPaused: {}", status.paused());
    println!("\tInterrupted: {}", status.interrupted());
    println!("\tEnded: {}", status.ended());
    println!("\tActive: {}", status.active());

    println!("\n\t* Debug register");
    let debug = reg.view().debug();
    println!("\tLite: {}", debug.lite());
    println!("\tVersion: {}", debug.version());
    println!("\tDMA state: {}", debug.state());
    println!("\tAXI Id: {}", debug.axi_id());
    println!("\tOutstanding writes: {}", debug.outstanding_writes());
    println!("\tRead error: {}", debug.read_error());
    println!("\tFifo error: {}", debug.fifo_error());
    println!(
        "\tRead last not set error: {}",
        debug.read_last_not_set_error()
    );

    println!("\n\t* TX info register");
    let tx_info = reg.view().tx_info();
    println!("\tSrc increment: {}", tx_info.src_increment());
    println!("\tDst increment: {}", tx_info.dst_increment());
    println!(
        "\tGenerate interrupt: {}",
        tx_info.interrupt_when_complete()
    );

    println!("\n\t* Other registers");
    println!(
        "\tCtrl block physical addr: {:?}",
        reg.view().ctrl_blk_addr()
    );
    println!(
        "\tSrc physical addr: {:?}",
        reg.view().src_addr()
    );
    println!(
        "\tDst physical addr: {:?}",
        reg.view().dst_addr()
    );
    println!("\tTx len: {}", reg.view().tx_len());
    println!("\tNext ctrl block: {:?}", reg.view().next_ctrl_blk_addr());
    println!();
}

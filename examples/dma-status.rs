extern crate bcm283x;

use bcm283x::dma;

mod util;

use std::env;

/// Print out some information about the DMA channels.
fn main() {
    unsafe {
        let dma_pages = dma::linux::DmaControlRegisters::new().expect("Couldn't load DMA pages");

        let global_enabled = dma_pages.global_enable_status();
        let global_interrupt = dma_pages.global_interrupt_status();

        if env::args().len() > 1 {
            let chan_num = env::args()
                .skip(1)
                .next()
                .unwrap()
                .parse::<usize>()
                .unwrap();
            util::print_channel(
                &dma::CHANNELS[chan_num],
                &dma_pages,
                global_enabled,
                global_interrupt,
            )
        } else {
            // show all
            for channel in dma::CHANNELS.iter() {
                util::print_channel(channel, &dma_pages, global_enabled, global_interrupt)
            }
        }
    }
}

extern crate bcm283x;
extern crate libc;
extern crate rpi_info;
extern crate vm_info;

use bcm283x::dma::{self, ControlBlockBuilder, RegisterSet, RegisterView, RegisterWriter};
use bcm283x::linux;

use std::{str, thread, time};

mod util;

/// An example that allocates two pages and copies some memory between them via DMA.
fn main() {
    unsafe {
        // Set up some memory to copy
        let mut src = linux::alloc_pages(1).unwrap();
        // actually mutable but it's hidden from the rust compiler, so not mut
        let dst = linux::alloc_pages(1).unwrap();

        let s = "hello world";

        src.write_volatile(s.as_bytes());

        // buffer to do our volatile reads into
        let mut buf: Vec<u8> = (0..s.len()).map(|_| 0_u8).collect();
        src.read_volatile(&mut buf[..]);
        println!("src before: {}", str::from_utf8(&buf[..]).unwrap());
        dst.read_volatile(&mut buf[..]);
        println!("dest before: {}", str::from_utf8(&buf[..]).unwrap());

        let dma_pages =
            dma::linux::DmaControlRegisters::new().expect("Could not set up DMA control pages");
        let dma_channel = &dma::CHANNELS[8];

        let mut chan_reg = dma_pages.channel_lite(dma_channel);
        {
            let mut writer = chan_reg.write();

            println!("Resetting channel");
            writer
                .ctrl_status()
                .reset(true)
                .write_register();
        }

        println!("\nChannel info after reset:");
        util::print_channel(
            &dma_channel,
            &dma_pages,
            dma_pages.global_enable_status(),
            dma_pages.global_interrupt_status(),
        );

        // make a control block
        let mut tx_info = dma::TransferInfoBuilderLite::new();
        tx_info
            .src_increment(true)
            .dst_increment(true)
            .interrupt_when_complete(false);

        let mut cb = dma::ControlBlockBuilderLite::new();
        cb.tx_info(tx_info)
            .src_addr(src.phys_addr())
            .dst_addr(dst.phys_addr())
            .tx_len(s.len() as u16);

        {
            let mut writer = chan_reg.write();

            // write the control block into memory with a known physical address to hand off to DMA
            let cb_mem = linux::alloc_pages(1).unwrap();
            writer.ctrl_blk_addr(&cb, &cb_mem);
        };

        println!("\nChannel info after writing only CB:");
        util::print_channel(
            &dma_channel,
            &dma_pages,
            dma_pages.global_enable_status(),
            dma_pages.global_interrupt_status(),
        );

        {
            let mut writer = chan_reg.write();

            println!("Starting DMA");
            writer
                .ctrl_status()
                .clear_dma_end(true)
                .clear_interrupt(true)
                .activate(true)
                .write_register();
        };

        println!("\nChannel info after start:");
        util::print_channel(
            &dma_channel,
            &dma_pages,
            dma_pages.global_enable_status(),
            dma_pages.global_interrupt_status(),
        );

        while chan_reg.view().ctrl_status().active() {
            println!("Still active...");
            thread::sleep(time::Duration::from_secs(1));
        }

        println!("\nChannel info after wait:");
        util::print_channel(
            &dma_channel,
            &dma_pages,
            dma_pages.global_enable_status(),
            dma_pages.global_interrupt_status(),
        );

        dst.read_volatile(&mut buf[..]);
        println!("dest after: {}", str::from_utf8(&buf[..]).unwrap());

        // in real code you would need to free the memory you used at some point
        // TODO free/munmap?
    }
}

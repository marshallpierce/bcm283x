The BCM2835, BCM2836, and BCM2837 chips from Broadcom are used in Raspberry Pi computers, and bundle various peripheral devices along with the ARM CPU. These peripherals do all sorts of fancy things (timers, DMA, etc) but are inaccessible or from typical userspace code. However, you need to use these peripherals to get good performance on many tasks on a Pi, especially time-sensitive ones like accurate signal sampling or generation. This is especially true when running a full OS like Linux (as you typically would on a Pi) because the kernel will do whatever it likes when scheduling your code.

This library reaches into physical memory to configure registers directly so that you can use these features from normal code. This is, of course, wildly dangerous, so the API does its best to protect you from shooting yourself in the foot. However, because you're bypassing the kernel, you still can get into trouble if you do things like try to take over a DMA channel that the kernel has claimed for its own use.

See https://www.raspberrypi.org/documentation/hardware/raspberrypi/bcm2835/README.md for more on the underlying hardware.

# Limitations

At the moment it's built on and intended for use with Linux, but much of the core pointer-wrangling is OS agnostic and could be used on bare metal without the physical/virtual memory mapping, etc. If this is your situation, file an issue and let's make it happen.

Also, the 32-bit nature of the platform is baked in because the relevant registers are all 32 bits wide anyway.
